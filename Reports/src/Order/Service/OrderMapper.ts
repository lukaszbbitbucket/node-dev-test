import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { Order } from '../Model/Order';
import { Product } from '../Model/Product';
import { Customer } from '../Model/Customer';

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;

  async getOrders() {
    const rawOrders = await this.repository.fetchOrders();
    const rawProducts = await this.repository.fetchProducts();
    const rawCustomers = await this.repository.fetchCustomers();

    return rawOrders.map(r =>
      OrderMapper.mapOrder(r, rawProducts, rawCustomers),
    );
  }

  private static mapOrder(
    rawOrder: any,
    rawProducts: any[],
    rawCustomers: any[],
  ) {
    const order = new Order();

    order.number = rawOrder.number;
    order.createdAt = rawOrder.createdAt;

    order.customer = new Customer();

    const rawCustomer = rawCustomers.find(r => r.id === rawOrder.customer);
    if (rawCustomer) {
      order.customer.id = rawCustomer.id;
      order.customer.firstName = rawCustomer.firstName;
      order.customer.lastName = rawCustomer.lastName;
    }

    order.products = rawOrder.products.map(id => {
      const product = new Product();
      product.id = id;

      const rawProduct = rawProducts.find(r => r.id === id);
      if (rawProduct) {
        product.name = rawProduct.name;
        product.price = rawProduct.price;
      }

      return product;
    });

    return order;
  }
}
