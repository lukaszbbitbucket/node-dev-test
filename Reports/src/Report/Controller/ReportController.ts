import { Controller, Get, Param } from '@nestjs/common';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { IBestSellers, IBestBuyers } from '../Model/IReports';

interface ProductsSummary {
  [productName: string]: {
    quantity: number;
    price: number;
  };
}

@Controller()
export class ReportController {
  constructor(private readonly orderMapper: OrderMapper) {}

  private async getOrdersForDate(date: string) {
    const orders = await this.orderMapper.getOrders();
    return orders.filter(o => o.createdAt === date);
  }

  @Get('/report/products/:date')
  async bestSellers(@Param('date') date: string): Promise<IBestSellers[]> {
    const orders = await this.getOrdersForDate(date);

    const summary: ProductsSummary = {};

    for (let order of orders) {
      for (let product of order.products) {
        if (!summary[product.name]) {
          summary[product.name] = { quantity: 1, price: product.price };
        } else {
          summary[product.name].quantity += 1;
        }
      }
    }

    const result = Object.entries(summary).map(([productName, info]) => ({
      productName,
      quantity: info.quantity,
      totalPrice: info.quantity * info.price,
    }));

    result.sort((a, b) => b.quantity - a.quantity);

    return result;
  }

  @Get('/report/customer/:date')
  async bestBuyers(@Param('date') date: string): Promise<IBestBuyers[]> {
    const orders = await this.getOrdersForDate(date);

    const prices: { [customerName: string]: number } = {};

    for (let order of orders) {
      const { customer } = order;
      const customerName = `${customer.firstName} ${customer.lastName}`;

      for (let product of order.products) {
        if (!prices[customerName]) {
          prices[customerName] = product.price;
        } else {
          prices[customerName] += product.price;
        }
      }
    }

    const result = Object.entries(prices).map(([customerName, totalPrice]) => ({
      customerName,
      totalPrice,
    }));

    result.sort((a, b) => b.totalPrice - a.totalPrice);

    return result;
  }
}
