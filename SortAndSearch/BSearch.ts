export class BSearch {
    private static instance = new BSearch();  

    static getInstance() {
        return BSearch.instance;
    }

    private _operations = 0;

    get operations() {
        return this._operations;
    }

    find(elem: number, xs: number[]): number {
        let lo = 0;
        let hi = xs.length - 1;

        while (lo <= hi) {
            const mid = Math.floor((lo + hi) / 2);
            this._operations += 1;

            if (xs[mid] < elem) {
                lo = mid + 1;
                continue;
            }
            if (xs[mid] > elem) {
                hi = mid - 1;
                continue;
            }

            return mid;
        }

        return -1;
    }
}
