// BSort implements quicksort algorithm.
export class BSort {
    static sort(xs: number[]) {
        BSort.doSort(xs, 0, xs.length - 1);
    }

    private static doSort(xs: number[], lo: number, hi: number) {
        if (hi <= lo) {
            return;
        }

        const p = BSort.partition(xs, lo, hi);

        BSort.doSort(xs, lo, p - 1);
        BSort.doSort(xs, p + 1, hi);
    }

    private static partition(xs: number[], lo: number, hi: number): number {
        const v = xs[hi];

        let i = lo;
        let j = lo;

        while (j <= hi)  {
            if (xs[j] < v) {
                BSort.swap(xs, i, j);
                i += 1;
            }
            j += 1;
        }

        BSort.swap(xs, i, hi);

        return i;
    }

    private static swap(xs: number[], i: number, j: number) {
        const temp = xs[j];
        xs[j] = xs[i];
        xs[i] = temp;
    }
}
