// ASort implements top-down mergesort algorithm.
export class ASort {
    static sort(xs: number[]) {
        const ys = [];
        ASort.doSort(xs, 0, xs.length - 1, ys);
    }

    private static doSort(xs: number[], lo: number, hi: number, ys: number[]) {
        if (hi <= lo) {
            return;
        }

        const mid = Math.floor(lo + (hi - lo) / 2);

        ASort.doSort(xs, lo, mid, ys);
        ASort.doSort(xs, mid + 1, hi, ys);

        ASort.merge(xs, lo, mid, hi, ys);
    }

    private static merge(xs: number[], lo: number, mid: number, hi: number, ys: number[]) {
       let i = lo;
       let j = mid + 1;

       for (let k = lo; k <= hi; k += 1) {
           ys[k] = xs[k];
       }

       for (let k = lo; k <= hi; k += 1) {
           if (i > mid || ys[j] < ys[i]) {
               xs[k] = ys[j];
               j += 1;
           } else {
               xs[k] = ys[i];
               i += 1;
           }
       }
    }
}
