import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

function testSort(name: string, alg: { sort: (xs: number[]) => void }) {
    const expected = [...unsorted];
    expected.sort((a, b) => a - b);

    const actual = [...unsorted];
    alg.sort(actual);

    const ok = expected.every((x, i) => x === actual[i]);
    console.log(`${name} => ${ok}`);
    if (!ok) {
        console.log(`  expected: ${JSON.stringify(expected)}`);
        console.log(`  actual:   ${JSON.stringify(actual)}`);
    }
}

testSort("ASort", ASort);
testSort("BSort", BSort);

const xs = [...unsorted];
xs.sort((a, b) => a - b);

elementsToFind.forEach(elem => {
    const idx = BSearch.getInstance().find(elem, xs);
    console.log(`${elem} => ${idx} (${BSearch.getInstance().operations})`);
});
